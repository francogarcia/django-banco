Sistema Banc�rio Simples usando Django

-------------------------------------------------------------------------------

Introdu��o ao Django e Configura��o

Ver doc/Tutorial.txt.

-------------------------------------------------------------------------------

Uso

Para criar as tabelas do banco de dados:

$ python manage.py makemigrations && python manage.py migrate && python manage.py syncdb

Para criar o administrador:

$ python manage.py createsuperuser

Para iniciar as tarefas de rotina do sistema:

$ python manage.py celery beat --loglevel=info

Para visualizar o log das tarefas:

$ python manage.py celery worker --loglevel=info

Para iniciar o servidor:

$ python manage.py runserver

Para acesso as p�ginas do sistema:

P�gina do administrador: http://127.0.0.1:8000/admin
P�gina para acesso p�blico: http://127.0.0.1:8000/banco/

-------------------------------------------------------------------------------
resu
Depend�ncias:

1) Celery:

Para processamento de tarefas dependentes de tempo.

Informa��es:
<http://docs.celeryproject.org/en/latest/getting-started/brokers/django.html>
<http://docs.celeryproject.org/en/2.5/django/first-steps-with-django.html>

Instala��o:

$ pip install django-celery

2) dateutil

Para opera��es com datas.

Informa��es:
<https://labix.org/python-dateutil>

Instala��o:
$ pip install python-dateutil

-------------------------------------------------------------------------------
