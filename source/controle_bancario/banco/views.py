# -*- coding: utf-8 -*-

import datetime
from django.db import IntegrityError
from django.http import HttpResponse
from django.shortcuts import render, render_to_response
from django.template import Context, Template, RequestContext
from django.utils import timezone

from banco.models import Agencia
from banco.models import Banco
from banco.models import Cidade
from banco.models import Cliente
from banco.models import ClienteInativo
from banco.models import Conta
from banco.models import ContaCliente
from banco.models import ContaCorrente
from banco.models import ContaPoupanca
from banco.models import Endereco
from banco.models import Estado
from banco.models import Transacao

#from banco.forms import CidadeForm
from banco.forms import EstadoForm
from banco.forms import TransacaoDepositoForm
from banco.forms import TransacaoSaqueForm
from banco.forms import TransacaoTransferenciaForm
from banco.forms import TransacaoSaldoForm

import logging
g_logger = logging.getLogger(__name__)


######################################
#          Constantes                #
######################################

STRING_MAX_LENGTH = 200

STRING_NAME_MAX_LENGTH = STRING_MAX_LENGTH
STRING_NAME_MIN_LENGTH = 1

STRING_ACCOUNT_LENGTH = 7

# 12.345.678-9
STRING_RG_LENGTH = 9 # apenas digitos
# 123.456.789-01
STRING_CPF_LENGTH = 11 # apenas digitos
# 00.000.000/0001-00
STRING_CNPJ_LENGTH = 14 # apenas digitos
# (XX)0000-0000
STRING_PHONE_LENGTH = 10 # apenas digitos
# 12345-678
STRING_CEP_LENGTH = 8 # apenas digitos

CURRENCY_MAX_DIGITS = 20
CURRENCY_DECIMAL_NUMBERS = 2

TIPO_CONTA_POUPANCA = 'P'
TIPO_CONTA_CORRENTE = 'C'

TIPO_TRANSACAO_DEPOSITO      = 'D'
TIPO_TRANSACAO_SAQUE         = 'S'
TIPO_TRANSACAO_TRANSFERENCIA = 'T'

######################################
#          Views                     #
######################################

# Create your views here.
def index(request):
  #return HttpResponse("Hello, world!")
  context = RequestContext(request)

  estados = Estado.objects.order_by('nome')
  # Bind entre 'estados' do HTML com a variavel estados do Python.
  cidades = Cidade.objects.order_by('nome')
  # Bind entre 'cidades' do HTML com a variavel cidades do Python.
  clientes = Cliente.objects.order_by('nome')
  # Bind entre 'clientes' do HTML com a variavel clientes do Python
  contas = Conta.objects.order_by('numero')
  # Bind entre 'contas' do HTML com a variavel contas do Python
  conta_clientes = ContaCliente.objects.order_by('conta')
  # Bind entre 'contaClientes' do HTML com a variavel contaClientes do Python
  context_dictionary = {'estados' : estados,
                        'cidades' : cidades,
                        'clientes' : clientes,
                        'contas' : contas,
                        'conta_clientes' : conta_clientes}

  return render_to_response('banco/index.html',
                            context_dictionary,
                            context)

def estados(request):
  context = RequestContext(request)

  estados = Estado.objects.order_by('nome')
  # Bind entre 'estados' do HTML com a variavel estados do Python.
  context_dictionary = {'estados' : estados}

  for estado in estados:
    estado.url = estado.nome.replace(' ', '_')

  return render_to_response('banco/estados/index.html',
                            context_dictionary,
                            context)


def cidades(request):
    context = RequestContext(request)

    cidades = Cidade.objects.order_by('nome')
    # Bind entre 'cidades' do HTML com a variavel cidades do Python
    context_dictionary = {'cidades' : cidades}

    for cidade in cidades:
        cidade.url = cidade.nome.replace(' ', '_')

    return render_to_response('banco/cidades/index.html',
                              context_dictionary,
                              context)

def clientes(request):
    context = RequestContext(request)

    clientes = Cliente.objects.order_by('nome')
    # Bind entre 'clientes do HTML com a variavel clientes do Python
    context_dictionary = {'clientes' : clientes}

    for cliente in clientes:
        cliente.url = cliente.nome.replace(' ', '_')

    return render_to_response('banco/clientes/index.html',
                              context_dictionary,
                              context)

def contas(request):
    context = RequestContext(request)

    contas = Conta.objects.order_by('numero')
    # Bind entre 'contas' do HTML com a variavel contas do Python
    context_dictionary = {'contas' : contas}

    return render_to_response('banco/contas/index.html',
                              context_dictionary,
                              context)

def bancos(request):
    context = RequestContext(request)

    bancos = Banco.objects.order_by('nome')
    # Bind entre 'bancos' do HTML com a variavel bancos do Python
    context_dictionary = {'bancos' : bancos}

    return render_to_response('banco/bancos/index.html',
                              context_dictionary,
                              context)

def agencias(request):
    context = RequestContext(request)

    agencias = Agencia.objects.order_by('nome')
    # Bind entre 'agencias' do HTML com a variavel agencias do Python
    context_dictionary = {'agencias' : agencias}

    return render_to_response('banco/bancos/agencias/index.html',
                              context_dictionary,
                              context)

def contaClientes(request):
    context = RequestContext(request)

    contaClientes = ContaCliente.objects.order_by('conta')
    # Bind entre 'contaClientes' do HTML com a variavel contas do Python
    context_dictionary = {'contaClientes' : contaClientes}

    return render_to_response('banco/contaClientes/index.html',
                              context_dictionary,
                              context)

def enderecos(request):
    context = RequestContext(request)

    enderecos = Endereco.objects.order_by('rua')

    # Bind entre 'enderecos' do HTML com a variavel enderecos do Python
    context_dictionary = {'enderecos' : enderecos}

    return render_to_response('banco/enderecos/index.html',
                              context_dictionary,
                              context)

def estado(request, nome_estado_url):
  context = RequestContext(request)

  nome_estado = nome_estado_url.replace('_', ' ')

  context_dictionary = {'nome_estado' : nome_estado}

  try:
    estado = Estado.objects.get(nome=nome_estado)
    context_dictionary['estado'] = estado

    cidades = Cidade.objects.filter(estado=estado)
    context_dictionary['cidades'] = cidades
  except Estado.DoesNotExist:
    pass

  return render_to_response('banco/estados/estado.html',
                            context_dictionary,
                            context)

def adicionar_estado(request):
  context = RequestContext(request)

  if request.method == 'POST':
    form = EstadoForm(request.POST)

    if (form.is_valid()):
      form.save(commit=True)

      return estados(request)
    else:
      print(form.errors)

  else:
    form = EstadoForm()

  return render_to_response('banco/estados/adicionar_estado.html', {'form' : form}, context)

def transacoes(request):
  return HttpResponse("Transacoes")

def transacao_saque(request):
  context = RequestContext(request)

  context_dictionary = {}

  return render_to_response('banco/contas/transacao/transacao_saque.html',
                            context_dictionary,
                            context)

def transacao_saque_form(request):
  context = RequestContext(request)

  if request.method == 'POST':
    form = TransacaoSaqueForm(request.POST)

    if (form.is_valid()):
      try:
        transacao = form.save(commit=False)
        transacao.tipo = TIPO_TRANSACAO_SAQUE
        transacao.data = timezone.now()
        transacao.save()

        #return transacao_saque(request)
        return render(request, 'banco/contas/transacao/transacao_saque.html', {'transacao' : transacao})
      except IntegrityError:
        transaction.rollback()
    else:
      g_logger.error(form.errors)
      return HttpResponse("Formulario incorreto.")

  else:
    form = TransacaoSaqueForm()

  return render_to_response('banco/contas/transacao/saque.html', {'form' : form}, context)

def transacao_deposito(request):
  context = RequestContext(request)

  context_dictionary = {}

  return render_to_response('banco/contas/transacao/transacao_deposito.html',
                            context_dictionary,
                            context)

def transacao_deposito_form(request):
  context = RequestContext(request)

  if request.method == 'POST':
    form = TransacaoDepositoForm(request.POST)

    if (form.is_valid()):
      try:
        transacao = form.save(commit=False)
        transacao.tipo = TIPO_TRANSACAO_DEPOSITO
        transacao.data = timezone.now()
        transacao.save()

        return render(request, 'banco/contas/transacao/transacao_deposito.html', {'transacao' : transacao})
      except IntegrityError:
        transaction.rollback()
    else:
      g_logger.error(form.errors)
      return HttpResponse("Formulario incorreto.")

  else:
    form = TransacaoDepositoForm()

  return render_to_response('banco/contas/transacao/deposito.html', {'form' : form}, context)

def transacao_transferencia(request):
  context = RequestContext(request)

  context_dictionary = {}

  return render_to_response('banco/contas/transacao/transacao_transferencia.html',
                            context_dictionary,
                            context)

def transacao_transferencia_form(request):
  context = RequestContext(request)

  if request.method == 'POST':
    form = TransacaoTransferenciaForm(request.POST)

    if (form.is_valid()):
      try:
        transacao = form.save(commit=False)
        transacao.tipo = TIPO_TRANSACAO_TRANSFERENCIA
        transacao.data = timezone.now()
        transacao.save()

        return render(request, 'banco/contas/transacao/transacao_transferencia.html', {'transacao' : transacao})
      except IntegrityError:
        transaction.rollback()
    else:
      g_logger.error(form.errors)
      return HttpResponse("Formulario incorreto.")

  else:
    form = TransacaoTransferenciaForm()

  return render_to_response('banco/contas/transacao/transferencia.html', {'form' : form}, context)

def clientes_inativos(request):
    context = RequestContext(request)

    clientes_inativos = ClienteInativo.objects.order_by('nome')
    # Bind entre 'cidades' do HTML com a variavel cidades do Python
    context_dictionary = {'clientes_inativos' : clientes_inativos}

    return render_to_response('banco/consultas/8_clientes_inativos.html',
                              context_dictionary,
                              context)

def transacao_saldo(request):
  context = RequestContext(request)

  context_dictionary = {}

  return render_to_response('banco/contas/transacao/transacao_saldo.html',
                            context_dictionary,
                            context)

# https://docs.djangoproject.com/en/dev/topics/db/queries/#lookups-that-span-relationships
# https://docs.djangoproject.com/en/dev/topics/db/queries/#spanning-multi-valued-relationships
def transacao_saldo_form(request):
  context = RequestContext(request)

  if request.method == 'POST':
    form = TransacaoSaldoForm(request.POST)

    if (form.is_valid()):
      try:
        transacao = form.save(commit=False)
        #conta = Conta.objects.filter(numero=transacao.numero, agencia==transacao.agencia.all())
        #transacao.data = timezone.now()

        return render(request, 'banco/contas/transacao/transacao_saldo.html', {'conta' : conta})
      except IntegrityError:
        transaction.rollback()
    else:
      g_logger.error(form.errors)
      return HttpResponse("Formulario incorreto.")

  else:
    form = TransacaoSaldoForm()

  return render_to_response('banco/contas/transacao/saldo.html', {'form' : form}, context)

# Simple sample pages

def hello(request):
  current_time = datetime.datetime.now()
  html = r"<html><body><h1>Simple Static Page</h1><p>Page rendered at: %s.</p></body></html>" % current_time

  return HttpResponse(html)

def hello_param(request, time_offset):
  try:
    time_offset_value = int(time_offset)
  except ValueError:
    raise Http404()

  # assert False
  current_time = datetime.datetime.now()
  new_time = current_time + datetime.timedelta(hours=time_offset_value)

  html = r"<html><body>%s + %s hour(s) = %s.</p></body></html>" % (current_time, time_offset_value, new_time)

  return HttpResponse(html)

def search_form(request):
  html = (r'<html>'
            r'<head>'
              r'<title>Form</title>'
            r'</head>'
            r'<body>'
              r'<form action="search/" method="get">'
                r'<input type="text" name="q"/>'
                r'<input type="submit" value="Search"/>'
              r'</form>'
            r'</body>'
          r'</html>')

  #return render(request, 'search_form.html')
  return HttpResponse(html)

def search(request):
  if 'q' in request.GET and request.GET['q']:
    #message = 'OK! Query: \"%s\".' % request.GET['q']

    #return HttpResponse(message)

    q = request.GET['q']
    #estados = Estado.objects.all()
    estados = Estado.objects.filter(nome__icontains=q)

    html = (r'<html>'
              r'<head>'
                r'<title>Form</title>'
              r'</head>'
              r'<body>'

                r'<p>Termo de busca: <strong>{{ q }}</strong></p>'

                r'{% if estados %}r'
                  r'<p>Resultado: {{ estados|length }} estado{{ estados|pluralize }}.</p>'
                  r'<ul>'
                    r'{% for estado in estados %}'
                      r'<li>{{ estado.nome }}</li>'
                    r'{% endfor %}'
                  r'</ul>'
                r'{% else %}'
                  r'<p>Nenhum resultado foi encontrado.</p>'
                r'{% endif %}'

              r'</body>'
            r'</html>')
    #return HttpResponse(html)

    # https://docs.djangoproject.com/en/dev/ref/templates/api/#using-the-template-system
    t = Template(html)
    c = Context({'q' : q, 'estados': estados})
    html = t.render(c)

    #return render(request, html, {'q' : q, 'estados': estados})
    return HttpResponse(html)

  else:
    message = "Empty form."
    return HttpResponse(message)

###############################################################################
# CONSULTAS                                                                   #
###############################################################################

# Consulta 2
def agencias_banco(request, nome_banco_url):
  context = RequestContext(request)

  nome_banco = nome_banco_url.replace('_', ' ')

  context_dictionary = {'nome_banco' : nome_banco}

  try:
    banco = Banco.objects.get(nome=nome_banco)
    context_dictionary['banco'] = banco

    agencias = Agencia.objects.filter(banco=banco)
    context_dictionary['agencias'] = agencias
  except Banco.DoesNotExist:
    pass

  return render_to_response('banco/bancos/agencias/agencias_banco.html',
                            context_dictionary,
                            context)

def contas_poupanca_cidade(request, nome_cidade_url):
  context = RequestContext(request)

  nome_cidade = nome_cidade_url.replace('_', ' ')

  context_dictionary = {'nome_cidade' : nome_cidade}

  try:
    cidade = Cidade.objects.get(nome=nome_cidade)
    context_dictionary['cidade'] = cidade

    contas = ContaPoupanca.objects.filter(clientes__endereco__cep__cidade = cidade)
    context_dictionary['contas'] = contas
  except Cidade.DoesNotExist:
    pass

  return render_to_response('banco/consultas/3_contas_poupanca_cidade.html',
                            context_dictionary,
                            context)

def contas_corrente_cidade(request, nome_cidade_url):
  context = RequestContext(request)

  nome_cidade = nome_cidade_url.replace('_', ' ')

  context_dictionary = {'nome_cidade' : nome_cidade}

  try:
    cidade = Cidade.objects.get(nome=nome_cidade)
    context_dictionary['cidade'] = cidade

    contas = ContaCorrente.objects.filter(clientes__endereco__cep__cidade = cidade)
    context_dictionary['contas'] = contas
  except Cidade.DoesNotExist:
    pass

  return render_to_response('banco/consultas/4_contas_corrente_cidade.html',
                            context_dictionary,
                            context)
