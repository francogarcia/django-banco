import inspect
from functools import wraps

from django.db import models
from django.db.models import Q, signals
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save, pre_delete, post_delete
from django.utils import timezone

import logging
g_logger = logging.getLogger(__name__)

#from django.db.models.signals import post_init # signals
# -*- encoding: utf-8 -*-
######################################
#          Constantes                #
######################################

STRING_MAX_LENGTH = 200

STRING_NAME_MAX_LENGTH = STRING_MAX_LENGTH
STRING_NAME_MIN_LENGTH = 1

STRING_ACCOUNT_LENGTH = 7

# 12.345.678-9
STRING_RG_LENGTH = 9 # apenas digitos
# 123.456.789-01
STRING_CPF_LENGTH = 11 # apenas digitos
# 00.000.000/0001-00
STRING_CNPJ_LENGTH = 14 # apenas digitos
# (XX)0000-0000
STRING_PHONE_LENGTH = 10 # apenas digitos
# 12345-678
STRING_CEP_LENGTH = 8 # apenas digitos

CURRENCY_MAX_DIGITS = 20
CURRENCY_DECIMAL_NUMBERS = 2

######################################
#        Tipos de Dados              #
######################################

# Tipos de dados: https://docs.djangoproject.com/en/1.7/ref/models/fields/#django.db.models.Field
# Tipos-padrao: https://docs.djangoproject.com/en/dev/ref/forms/fields/#built-in-field-classes

# https://docs.djangoproject.com/en/dev/ref/models/fields/
# https://docs.djangoproject.com/en/dev/howto/custom-model-fields/

# TODO: adicionar mascaras para validcao (https://docs.djangoproject.com/en/dev/ref/validators/)

# Alternativa: validator

class EnumField(models.Field):
  def __init__(self, *args, **kwargs):
    super(EnumField, self).__init__(*args, **kwargs)
    if not self.choices:
      raise AttributeError('EnumField requires `choices` attribute.')

  def db_type(self, connection):
    if connection.settings_dict['ENGINE'] == 'django.db.backends.mysql':
      return "enum(%s)" % ','.join("'%s'" % k for (k, _) in self.choices)
    else:
      return "char"

# Modelo com triggers
# https://djangosnippets.org/snippets/2124/

def autoconnect(cls):
    """
    Class decorator that automatically connects pre_save / post_save signals on
    a model class to its pre_save() / post_save() methods.
    """
    issignal = lambda x: isinstance(x,signals.Signal)
    allsignals = inspect.getmembers(signals, issignal)
    def connect(signal, func):
        cls.func = staticmethod(func)
        @wraps(func)
        def wrapper(sender, **kwargs):
            return func(kwargs.get('instance'))
        signal.connect(wrapper, sender=cls)
        return wrapper

    for (name, method) in allsignals:
        if hasattr(cls, name):
            setattr(cls, name, connect(method, getattr(cls, name)))

    return cls

######################################
#  Enumeracoes                       #
######################################

TIPO_CONTA_POUPANCA = 'P'
TIPO_CONTA_CORRENTE = 'C'
TIPO_CONTA = ((TIPO_CONTA_POUPANCA, 'Poupanca'),
              (TIPO_CONTA_CORRENTE, 'Corrente'))

TIPO_TRANSACAO_DEPOSITO      = 'D'
TIPO_TRANSACAO_SAQUE         = 'S'
TIPO_TRANSACAO_TRANSFERENCIA = 'T'
TIPO_TRANSACAO = ((TIPO_TRANSACAO_DEPOSITO, 'Deposito'),
                  (TIPO_TRANSACAO_SAQUE, 'Saque'),
                  (TIPO_TRANSACAO_TRANSFERENCIA, 'Transferencia'))

######################################
#  Managers                          #
######################################

# <https://docs.djangoproject.com/en/dev/topics/db/managers/>

#class TransacaoManager(models.Manager):



######################################
#  Modelos (classes) do projeto      #
######################################

# https://docs.djangoproject.com/en/dev/topics/db/models/#relationships
# Relacionamentos:
# 1 para 1          : OneToOneField
# 1 para Muitos     : ForeignKey
# Muitos para Muitos: ManyToManyField
#                     ManyToManyField(through='AssociationClass')

class Estado(models.Model):
  nome   = models.CharField(max_length=STRING_NAME_MAX_LENGTH)

  def __str__(self):
    return self.nome

class Cidade(models.Model):
  estado  = models.ForeignKey(Estado)
  nome    = models.CharField(max_length=STRING_NAME_MAX_LENGTH)

  def __str__(self):
    return self.nome

class CEP(models.Model):
  codigo = models.CharField(max_length=STRING_CEP_LENGTH)
  cidade = models.ForeignKey(Cidade)
  rua    = models.CharField(max_length=STRING_NAME_MAX_LENGTH)

  def __str__(self):
    return self.codigo

class Endereco(models.Model):
  cep         = models.ForeignKey(CEP)
  numero      = models.IntegerField()
  complemento = models.CharField(max_length=STRING_NAME_MAX_LENGTH, blank=True)
  telefone    = models.CharField(max_length=STRING_PHONE_LENGTH)

  def __str__(self):
    endereco = u'%s, %i' % (self.cep.rua, self.numero)
    if (self.complemento):
      endereco += u', %s' % (self.complemento)
    return endereco

class Banco(models.Model):
  nome   = models.CharField(max_length=STRING_NAME_MAX_LENGTH)
  #codigo = models.AutoField(primary_key=True)
  codigo = models.IntegerField(primary_key=True)
  cnpj   = models.CharField(verbose_name='CNPJ', max_length=STRING_CNPJ_LENGTH)

  def __str__(self):
      return self.nome + ' (CNPJ ' + self.cnpj + ')'

class Agencia(models.Model):
  banco    = models.ForeignKey(Banco)
  nome     = models.CharField(max_length=STRING_NAME_MAX_LENGTH)
  numero   = models.IntegerField()
  endereco = models.OneToOneField(Endereco)

  def __str__(self):
    return self.nome

class Cliente(models.Model):
  cpf                  = models.CharField(max_length=STRING_CPF_LENGTH)
  rg                   = models.CharField(max_length=STRING_RG_LENGTH)
  nome                 = models.CharField(max_length=STRING_NAME_MAX_LENGTH)
  sobrenome            = models.CharField(max_length=STRING_NAME_MAX_LENGTH)
  endereco             = models.OneToOneField(Endereco)
  data_inicial_moradia = models.DateField()

  def __str__(self):
    return self.nome + ' ' + self.sobrenome

class ClienteInativo(models.Model):
  cpf                  = models.CharField(max_length=STRING_CPF_LENGTH)
  rg                   = models.CharField(max_length=STRING_RG_LENGTH)
  nome                 = models.CharField(max_length=STRING_NAME_MAX_LENGTH)
  sobrenome            = models.CharField(max_length=STRING_NAME_MAX_LENGTH)
  endereco             = models.OneToOneField(Endereco)

  def __str__(self):
    return self.nome + ' ' + self.sobrenome

# Para forcar a remocao [1/2]
#from django.utils.dateparse import parse_datetime
#import pytz

@autoconnect
class Conta(models.Model):
  tipo = EnumField(choices=TIPO_CONTA)
  agencia          = models.ManyToManyField(Agencia)
  clientes         = models.ManyToManyField(Cliente, through='ContaCliente')
  numero           = models.CharField(max_length=STRING_ACCOUNT_LENGTH)
  data_abertura    = models.DateTimeField()
  saldo            = models.DecimalField(max_digits=CURRENCY_MAX_DIGITS, decimal_places=CURRENCY_DECIMAL_NUMBERS)
  saldo_zero_desde = models.DateTimeField(null=True, default=None)

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    g_logger.info("conta__save" + str(self.tipo))
    if self.saldo == 0:
      # Altera o timestamp apenas caso o saldo anterior nao fosse zero.
      if (self.saldo_zero_desde is None):
        self.saldo_zero_desde = timezone.now()
        # Para forcar a remocao [2/2]
        #naive = parse_datetime("2013-11-10 10:28:45")
        #self.saldo_zero_desde = pytz.timezone("America/Sao_Paulo").localize(naive, is_dst=None)
        #g_logger.info(str(self.saldo_zero_desde))
    else:
      self.saldo_zero_desde = None
      #g_logger.info(self.saldo)
    super(Conta, self).save(force_insert, force_update, *args, **kwargs)

  def delete(self):
    g_logger.info("conta__delete")
    super(Conta, self).delete()

  def pre_save(self):
    g_logger.info("Conta__presave")

  def pre_delete(self):
    g_logger.info("conta__pre_delete")

  def post_delete(self):
    g_logger.info("conta__post_delete: ")
    # Movido para signals.py
    # Razao: Django nao permite o acesso aos atributos removidos
    #clientes = self.clientes
    ## Remove clientes sem conta.
    #for cliente in clientes:
      #contas = ContaCliente.objects.get(cliente=cliente)
      #if (contas.len() == 0):
        ## Todas as contas foram removidas.
        #try:
          #cliente.delete()
          #g_logger.info("Removeu o cliente inativo " + str(cliente))
        #except IntegrityError:
          #transaction.rollback()

  def __str__(self):
    return self.numero

  # https://stackoverflow.com/questions/8952542/manytomanyfield-with-through-on-an-abstract-model
  #class Meta:
    #abstract = True

@autoconnect
class ContaPoupanca(Conta):
  def __str__(self):
    return Conta.__str__(self) + ' (' + self.tipo + ')'

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    self.tipo = TIPO_CONTA_POUPANCA
    super(ContaPoupanca, self).save(force_insert, force_update, *args, **kwargs)

  #class Meta(Conta.Meta):
    # se necessario

@autoconnect
class ContaCorrente(Conta):
  limite = models.DecimalField(max_digits=CURRENCY_MAX_DIGITS, decimal_places=CURRENCY_DECIMAL_NUMBERS)

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    self.tipo = TIPO_CONTA_CORRENTE
    super(ContaCorrente, self).save(force_insert, force_update, *args, **kwargs)

  #class Meta(Conta.Meta):
    # se necessario

class ContaCliente(models.Model):
  conta   = models.ForeignKey(Conta)#, related_name="%(app_label)s_%(class)s_related")
  cliente = models.ForeignKey(Cliente)
  titular = models.BooleanField(default=False)

  def __str__(self):
    return self.conta.numero + ' (Titular: ' + str(self.titular) + ')'

class Transacao(models.Model):
  tipo    = EnumField(choices=TIPO_TRANSACAO)
  destino = models.ForeignKey(ContaCliente, null=True, related_name="%(app_label)s_%(class)s_Destino")
  data    = models.DateTimeField()
  valor   = models.DecimalField(max_digits=CURRENCY_MAX_DIGITS, decimal_places=CURRENCY_DECIMAL_NUMBERS)

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    #g_logger.info("tipo = " + str(self.tipo))
    super(Transacao, self).save(force_insert, force_update, *args, **kwargs)

  def __str__(self):
      return str(self.tipo) + ' ' + self.destino.cliente.nome

class TransacaoSaque(Transacao):
  origem = models.ForeignKey(Cliente)

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    self.tipo = TIPO_TRANSACAO_SAQUE
    super(TransacaoSaque, self).save(force_insert, force_update, *args, **kwargs)

  def __str__(self):
      return Transacao.__str__(self)

class TransacaoDeposito(Transacao):
  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    self.tipo = TIPO_TRANSACAO_DEPOSITO
    super(TransacaoDeposito, self).save(force_insert, force_update, *args, **kwargs)

  def __str__(self):
      return Transacao.__str__(self)

class TransacaoTransferencia(Transacao):
  origem       = models.ForeignKey(ContaCliente, related_name="%(app_label)s_%(class)s_Origem")
  valor_origem = models.DecimalField(max_digits=CURRENCY_MAX_DIGITS, decimal_places=CURRENCY_DECIMAL_NUMBERS)

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    self.tipo = TIPO_TRANSACAO_TRANSFERENCIA
    super(TransacaoTransferencia, self).save(force_insert, force_update, *args, **kwargs)

  def __str__(self):
      return Transacao.__str__(self)+ ' <- ' + self.origem.cliente.nome

class ContaInativa(models.Model):
  tipo = EnumField(choices=TIPO_CONTA)
  agencia          = models.ManyToManyField(Agencia)
  clientes         = models.ManyToManyField(Cliente)
  numero           = models.CharField(max_length=STRING_ACCOUNT_LENGTH)
  data_abertura    = models.DateTimeField()
  data_remocao     = models.DateTimeField()

  def save(self, force_insert=False, force_update=False, *args, **kwargs):
    self.data_remocao = timezone.now()
    super(ContaInativa, self).save(force_insert, force_update, *args, **kwargs)

  def __str__(self):
    return self.numero
