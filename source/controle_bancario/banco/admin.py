# <https://docs.djangoproject.com/en/1.7/ref/contrib/admin/>
# Rollback: <https://docs.djangoproject.com/en/dev/topics/db/transactions/>

from django.contrib import admin

from banco.models import Agencia
from banco.models import Banco
from banco.models import CEP
from banco.models import Cidade
from banco.models import Cliente
from banco.models import ClienteInativo
from banco.models import Conta
from banco.models import ContaCliente
from banco.models import ContaCorrente
from banco.models import ContaPoupanca
from banco.models import ContaInativa
from banco.models import Endereco
from banco.models import Estado
from banco.models import Transacao
from banco.models import TransacaoDeposito
from banco.models import TransacaoSaque
from banco.models import TransacaoTransferencia

# Register your models here.
# Registra o gerenciamento de bancos na pagina do administrador.
# A pagina gerada sera criada pelo Django.
#admin.site.register(Banco)
# Registra o gerenciamento de agencias diretamente na pagina do administrador.
#admin.site.register(Agencia)
# Idem para demais.
#admin.site.register(Agencia)
#admin.site.register(Banco)
#admin.site.register(Cidade)
##admin.site.register(Conta) # TODO: tentar usar classe abstrata
#admin.site.register(ContaCliente)
#admin.site.register(ContaCorrente)
#admin.site.register(ContaPoupanca)
#admin.site.register(Cliente)
#admin.site.register(Endereco)
#admin.site.register(Estado)
#admin.site.register(Transacao)

class AgenciaInline(admin.StackedInline):
  model = Agencia
  extra = 1

class CEPInline(admin.StackedInline):
  model = CEP
  extra = 0

class CidadeInline(admin.StackedInline):
  model = Cidade
  extra = 0

class EstadoInline(admin.StackedInline):
  model = Estado
  extra = 0

class EnderecoInline(admin.StackedInline):
  model = Endereco
  extra = 1

class ContaInline(admin.StackedInline):
  model = Conta
  extra = 1

class ContaClienteInline(admin.TabularInline):
  model = ContaCliente
  extra = 1

#class ContaPoupancaInline(admin.StackedInline):
  #model = ContaPoupanca
  #extra = 1

#class ContaCorrenteInline(admin.StackedInline):
  #model = ContaCorrente
  #extra = 1

#class ClienteInline(admin.StackedInline):
  #model = Cliente
  #extra = 1

# Cria uma pagina automatica para o cadastro de bancos.
# Os campos da pagina sao definidos pelo atributo fieldsets.
class BancoAdmin(admin.ModelAdmin):
  fieldsets = [
                # (None, {'fields': ['codigo']}),
                ('Informacao Geral', {'fields': ['codigo', 'nome', 'cnpj']}),
  ]
  inlines = [AgenciaInline]
  list_display = ('codigo', 'nome', 'cnpj')
  search_fields = ('cnpj', )
admin.site.register(Banco, BancoAdmin)

class CEPAdmin(admin.ModelAdmin):
  fields = ['codigo', 'rua', 'cidade']
  inlines = [EnderecoInline]
  list_display = ['codigo', 'get_estado', 'cidade', 'rua']
  search_fields = ['codigo', 'rua']
  list_order = ['cep']
  def get_estado(self, obj): # Para usar atributo de chave estrangeira.
    return obj.cidade.estado
  get_estado.short_description = 'Estado'
  get_estado.admin_order_field = 'cidade__estado'
admin.site.register(CEP, CEPAdmin)

class CidadeAdmin(admin.ModelAdmin):
  fields = ['nome', 'estado']
  inlines = [CEPInline]
  list_display = ('nome', 'estado')
  search_fields = ('nome', 'estado')
  list_filter = ('estado', )
  extra = 0
admin.site.register(Cidade, CidadeAdmin)

class EstadoAdmin(admin.ModelAdmin):
  fields = ['nome']
  inlines = [CidadeInline]
  search_fields = ('nome', )
admin.site.register(Estado, EstadoAdmin)

class EnderecoAdmin(admin.ModelAdmin):
  fields = ['cep', 'numero', 'complemento', 'telefone']
  list_display = ('cep', 'numero', 'get_cidade', 'get_estado')
  search_fields = ('numero', 'cep')
  list_filter = ('cep', 'numero', 'cep__cidade', 'cep__cidade__estado')
  ordering = ('cep', 'numero')
  def get_cidade(self, obj): # Para usar atributo de chave estrangeira.
    return obj.cep.cidade
  get_cidade.short_description = 'Cidade'
  get_cidade.admin_order_field = 'cep__cidade'
  def get_estado(self, obj): # Para usar atributo de chave estrangeira.
    return obj.cep.cidade.estado
  get_estado.short_description = 'Estado'
  get_estado.admin_order_field = 'cep__cidade__estado'
admin.site.register(Endereco, EnderecoAdmin)

class ClienteAdmin(admin.ModelAdmin):
  fields = ['nome', 'sobrenome', 'endereco', 'cpf', 'rg', 'data_inicial_moradia']
  #inlines = [ContaClienteInline]
  list_display = ('nome', 'sobrenome')
admin.site.register(Cliente, ClienteAdmin)

class ClienteInativoAdmin(admin.ModelAdmin):
  fields = ['nome', 'sobrenome', 'endereco', 'cpf', 'rg']
  list_display = ('nome', 'sobrenome')
admin.site.register(ClienteInativo, ClienteInativoAdmin)

#class ContaAdmin(admin.ModelAdmin):
  #fields = ['agencia', 'numero']
  #inlines = [ContaClienteInline, ContaPoupancaInline, ContaCorrenteInline]
  #filter_horizontal = ['agencia', 'clientes']
  #search_fields = ('numero', 'agencia')
  #list_filter = ('agencia', )
#admin.site.register(Conta, ContaAdmin)

class ContaInativaAdmin(admin.ModelAdmin):
  fields = ['agencia', 'numero', 'tipo', 'clientes', 'data_abertura', 'data_remocao']
  filter_horizontal = ['agencia', 'clientes']
  search_fields = ('numero', 'agencia')
  list_filter = ('agencia', )
admin.site.register(ContaInativa, ContaInativaAdmin)

class ContaCorrenteAdmin(admin.ModelAdmin):
  fields = ['agencia', 'numero', 'data_abertura', 'saldo', 'limite']# 'tipo']
  inlines = [ContaClienteInline]
  list_display = ('numero', ) #, 'get_banco', 'get_agencia')
  filter_horizontal = ['agencia', 'clientes']
  search_fields = ('numero',) # 'agencia', 'cliente')
  list_filter = ('agencia', 'agencia__banco')
  #def get_banco(self, obj): # Para usar atributo de chave estrangeira.
    #return obj.agencia.banco.nome
  #get_banco.short_description = 'Banco'
  #get_banco.admin_order_field = 'agencia__banco__nome'
  #def get_agencia(self, obj): # Para usar atributo de chave estrangeira.
    #return obj.agencia.nome
  #get_agencia.short_description = 'Agencia'
  #get_agencia.admin_order_field = 'agencia__nome'
admin.site.register(ContaCorrente, ContaCorrenteAdmin)

class ContaPoupancaAdmin(admin.ModelAdmin):
  fields = ['agencia', 'numero', 'data_abertura', 'saldo']# 'tipo']
  inlines = [ContaClienteInline]
  list_display = ('numero', ) # 'get_agencias') #, agencia)
  filter_horizontal = ['agencia', 'clientes']
  search_fields = ('numero',) # 'agencia', 'cliente')
  list_filter = ('agencia', )
  #def get_agencias(self, obj):
    #return '\n'.join([a.agencias for a in obj.agencia.all()])
admin.site.register(ContaPoupanca, ContaPoupancaAdmin)

#class TransacaoAdmin(admin.ModelAdmin):
  #fields = ['tipo', 'origem', 'destino', 'data', 'valor']
  #list_display = ('tipo', 'data', 'origem', 'destino')
  #search_fields = ('tipo', 'data')
  #list_filter = ('tipo', 'data', 'origem', 'destino')
  #ordering = ('data', )
#admin.site.register(Transacao, TransacaoAdmin)

class TransacaoDepositoAdmin(admin.ModelAdmin):
  fields = ['destino', 'data', 'valor']
  list_display = ('tipo', 'data', 'destino')
  search_fields = ('tipo', 'data')
  list_filter = ('tipo', 'data', 'destino')
  ordering = ('data', )
  # Atualiza o saldo antes da persistencia em banco.
  def save_model(self, request, obj, form, change):
    # Obtem os dados do formulario
    form_data = form.cleaned_data
    # Obtem o valor do deposito
    obj.destino.conta.saldo += form_data['valor']
    # Salva o novo saldo da conta
    obj.destino.conta.save()
    try:
      # Salve o objeto da transacao de deposito
      obj.save()
    except IntegrityError:
      transaction.rollback()
admin.site.register(TransacaoDeposito, TransacaoDepositoAdmin)

class TransacaoSaqueAdmin(admin.ModelAdmin):
  fields = ['origem', 'destino', 'data', 'valor']
  list_display = ('tipo', 'data', 'destino')
  search_fields = ('tipo', 'data')
  list_filter = ('tipo', 'data', 'destino')
  ordering = ('data', )
  # Atualiza o saldo antes da persistencia em banco.
  def save_model(self, request, obj, form, change):
    # Obtem os dados do formulario
    form_data = form.cleaned_data
    # Obtem o valor do saque
    obj.destino.conta.saldo -= form_data['valor']
    try:
      # Salva o novo saldo da conta
      obj.destino.conta.save()
      # Salve o objeto da transacao de saque
      obj.save()
    except IntegrityError:
      transaction.rollback()
admin.site.register(TransacaoSaque, TransacaoSaqueAdmin)

class TransacaoTransferenciaAdmin(admin.ModelAdmin):
  fields = ['origem', 'destino', 'data', 'valor', 'valor_origem']
  list_display = ('tipo', 'data', 'origem', 'destino')
  search_fields = ('tipo', 'data')
  list_filter = ('tipo', 'data', 'origem', 'destino')
  ordering = ('data', )
  # Atualiza o saldo antes da persistencia em banco.
  def save_model(self, request, obj, form, change):
    # Obtem os dados do formulario
    form_data = form.cleaned_data
    # Obtem o valor da transferencia
    valor = form_data['valor']
    obj.destino.conta.saldo += valor
    valor_origem = form_data['valor_origem']
    obj.origem.conta.saldo  -= valor_origem
    try:
    # Salva o novo saldo da conta
      obj.destino.conta.save()
      obj.origem.conta.save()
      # Salve o objeto da transacao de saque
      obj.save()
    except IntegrityError:
      transaction.rollback()
admin.site.register(TransacaoTransferencia, TransacaoTransferenciaAdmin)
