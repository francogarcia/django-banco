from django.apps import AppConfig

class BancoConfig(AppConfig):
    name = 'banco'

    def ready(self):
        import banco.signals

