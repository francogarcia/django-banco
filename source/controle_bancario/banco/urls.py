from django.conf.urls import patterns, url

from banco import views

# Python regex (<http://www.djangoproject.com/r/python/re-module/>)
# .           qualquer caractere
# \d          qualquer digito (unico)
# [A-Z]       qualquer caractere entre A-Z
# [a-z]       qualquer caractere entre a-z
# # [A-Za-z]  qualquer caractere entre A-Z (maiusculo ou minusculo)
# +           uma ou mais ocorrencias da expressao anterior (exemplo: \d+ - um ou mais digitos)
# ?           zero ou uma ocorrencias da expressao anterior
# *           zero ou mais ocorrencias da expressao anterior
# [^/]+       um ou mais caracteres ate / (nao incluida)
# {x, y}      qualquer numero de ocorrencias entre x e y (inclusive)

urlpatterns = patterns('',
  # /banco/
  url(r'^$', views.index, name='index'), # r: raw string (considera '\' como '\', nao como escape)
                                         # ': inicio da string
                                         # $: fim da string
                                         # neste caso, portanto, o endereco base deve ser vazio

  # /banco/hello/
  url(r'^hello/$', views.hello, name='hello'),

  # /banco/estados/
  url(r'^estados/$', views.estados, name='estados'),

  # /banco/cidades/
  url(r'^cidades/$', views.cidades, name='cidades'),

  # /banco/clientes
  url(r'^clientes/$', views.clientes, name='clientes'),

  # /banco/contas
  url(r'^contas/$', views.contas, name='contas'),

  # /banco/contaClientes
  url(r'^contaClientes/$', views.contaClientes, name='contaClientes'),

  # /banco/enderecos
  url(r'^enderecos/$', views.enderecos, name='enderecos'),

  # Consulta 1
  # /banco/bancos
  url(r'^bancos/$', views.bancos, name='bancos'),

  # /banco/bancos/agencias
  url(r'^bancos/agencias/$', views.agencias, name='agencias'),

  # Consulta 2
  # /banco/bancos/Banco_1/agencias/
  # exemplo: http://127.0.0.1:8000/banco/bancos/Banco_1/agencias/
  url(r'^bancos/(?P<nome_banco_url>\w+)/agencias/$', views.agencias_banco, name='agencias_banco'),

  # Consulta 3
  # http://127.0.0.1:8000/banco/contas/poupanca/cidade/Sao_Paulo/
  # Nota: colocar acento.
  url(r'^contas/poupanca/cidade/(?P<nome_cidade_url>\w+)/$', views.contas_poupanca_cidade, name='contas_poupanca_cidade'),

  # Consulta 4
  # http://127.0.0.1:8000/banco/conta/corrente/cidade/Sao_Paulo/
  # Nota: colocar acento.
  url(r'^contas/corrente/cidade/(?P<nome_cidade_url>\w+)/$', views.contas_corrente_cidade, name='contas_corrente_cidade'),

  # Consulta 5
  # http://127.0.0.1:8000/banco/contas/transacao/saque
  url(r'^contas/transacao/saque/$', views.transacao_saque_form, name='transacao_saque_form'),
  url(r'^contas/transacao/transacao_saque/$', views.transacao_saque, name='transacao_saque'),

  # Consulta 6
  # http://127.0.0.1:8000/banco/contas/transacao/deposito
  url(r'^contas/transacao/deposito/$', views.transacao_deposito_form, name='transacao_deposito_form'),
  url(r'^contas/transacao/transacao_deposito/$', views.transacao_deposito, name='transacao_deposito'),

  # Consulta 7
  # http://127.0.0.1:8000/banco/contas/transacao/transferencia
  url(r'^contas/transacao/transferencia/$', views.transacao_transferencia_form, name='transacao_transferencia_form'),
  url(r'^contas/transacao/transacao_transferencia/$', views.transacao_transferencia, name='transacao_transferencia'),

  # Consulta 8
  # http://127.0.0.1:8000/banco/clientes/inativos
  url(r'^clientes/inativos/$', views.clientes_inativos, name='clientes_inativos'),

  # Consulta 9
  # http://127.0.0.1:8000/banco/contas/transacao/saldo/
  url(r'^contas/transacao/saldo/$', views.transacao_saldo_form, name='transacao_saldo_form'),
  url(r'^contas/transacao/transacao_saldo/$', views.transacao_saldo, name='transacao_saldo'),

  # /banco/estados/estado
  # exemplo: http://127.0.0.1:8000/banco/estados/Minas_Gerais/
  url(r'^estados/(?P<nome_estado_url>\w+)/$', views.estado, name='estado'),

  # /banco/estados/adicionar_estado
  url(r'^estados/adicionar_estado/$', views.adicionar_estado, name='adicionar_estado'),

  # Pretty URLs
  # /banco/hello/forward/1
  url(r'^hello/forward/(\d+)/$', views.hello_param, name='time_param'),

  # Forms
  # /banco/hello/form/
  url(r'^hello/form/$', views.search_form),
  url(r'^hello/form/search/$', views.search),

  # /banco/contas/1/
  #url(r'^(?P<argument>\d+)/$', views.contas, name='contas'),


)
