import datetime

from django.conf import settings
from django.db.models.signals import pre_save, post_save, pre_delete, post_delete
from django.core.signals import request_finished
from django.dispatch import receiver

from banco.models import Agencia
from banco.models import Banco
from banco.models import Cidade
from banco.models import Cliente
from banco.models import ClienteInativo
from banco.models import Conta
from banco.models import ContaCliente
from banco.models import ContaCorrente
from banco.models import ContaPoupanca
from banco.models import Endereco
from banco.models import Estado
from banco.models import Transacao

from banco.admin import TransacaoDepositoAdmin
from banco.admin import TransacaoSaqueAdmin
from banco.admin import TransacaoTransferenciaAdmin

import logging
g_logger = logging.getLogger(__name__)

# <https://docs.djangoproject.com/en/dev/ref/signals/>

@receiver(request_finished)
def on_request_started(sender, **kwargs):
  g_logger.info("HTML request started!")
  #g_logger.debug("HTML request started!")
  #g_logger.warning("HTML request started!")
  #g_logger.error("HTML request started!")

@receiver(request_finished)
def on_request_finished(sender, **kwargs):
  g_logger.info("HTML request finished!")

@receiver(pre_delete ,sender=Conta)
def conta_on_pre_delete(sender, instance, *args, **kwargs):
  g_logger.info("conta_on_pre_delete: " + instance.tipo)
  clientes = instance.clientes
  # Remove clientes sem conta.
  for cliente in clientes.all():
    contas = ContaCliente.objects.filter(cliente=cliente)
    g_logger.info("numero de contas: " + str(len(contas)))
    if (len(contas) == 1):
      # Todas as contas foram removidas.
      try:
        # Adiciona o cliente removido ao histórico.
        cliente_inativo = ClienteInativo(cpf=cliente.cpf,
                                         rg=cliente.rg,
                                         nome=cliente.nome,
                                         sobrenome=cliente.sobrenome,
                                         endereco=cliente.endereco)

        cliente.delete()
        g_logger.info("Removeu o cliente inativo " + str(cliente))

        cliente_inativo.save()
      except IntegrityError:
        transaction.rollback()
  g_logger.info("conta_on_pre_delete: END")

#@receiver(post_delete ,sender=Conta)
#def conta_on_post_delete(sender, instance, *args, **kwargs):
  #g_logger.info("conta_on_post_delete: " + instance.tipo)
  #g_logger.info("conta_on_post_delete: END")

