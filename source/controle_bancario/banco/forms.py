# <https://docs.djangoproject.com/en/dev/topics/forms/modelforms/#selecting-the-fields-to-use>

from django import forms

from django.contrib.admin.widgets import AdminDateWidget

from banco.models import Agencia
from banco.models import Banco
from banco.models import Cidade
from banco.models import Cliente
from banco.models import Conta
from banco.models import ContaCliente
from banco.models import ContaCorrente
from banco.models import ContaPoupanca
from banco.models import Endereco
from banco.models import Estado
from banco.models import Transacao
from banco.models import TransacaoDeposito
from banco.models import TransacaoSaque
from banco.models import TransacaoTransferencia

import logging
g_logger = logging.getLogger(__name__)

######################################
#          Constantes                #
######################################

STRING_MAX_LENGTH = 200

STRING_NAME_MAX_LENGTH = STRING_MAX_LENGTH
STRING_NAME_MIN_LENGTH = 1

STRING_ACCOUNT_LENGTH = 7

# 12.345.678-9
STRING_RG_LENGTH = 9 # apenas digitos
# 123.456.789-01
STRING_CPF_LENGTH = 11 # apenas digitos
# 00.000.000/0001-00
STRING_CNPJ_LENGTH = 14 # apenas digitos
# (XX)0000-0000
STRING_PHONE_LENGTH = 10 # apenas digitos
# 12345-678
STRING_CEP_LENGTH = 8 # apenas digitos

CURRENCY_MAX_DIGITS = 20
CURRENCY_DECIMAL_NUMBERS = 2

TIPO_CONTA_POUPANCA = 'P'
TIPO_CONTA_CORRENTE = 'C'

TIPO_TRANSACAO_DEPOSITO      = 'D'
TIPO_TRANSACAO_SAQUE         = 'S'
TIPO_TRANSACAO_TRANSFERENCIA = 'T'

######################################
#        Formularios                 #
######################################

class EstadoForm(forms.ModelForm):
  #nome = forms.CharField(max_length=STRING_MAX_LENGTH, help_text="Favor entrar com o nome do estado.")

  class Meta:
    model = Estado
    fields = '__all__'

class TransacaoSaqueForm(forms.ModelForm):
  # Validacao
  #def clean(self):
  #  return self.cleaned_data

  class Meta:
    model = TransacaoSaque
    #fields = '__all__' # Opcao 1
    exclude = ['data', 'tipo']

class TransacaoDepositoForm(forms.ModelForm):
  # Validacao
  #def clean(self):
  #  return self.cleaned_data

  class Meta:
    model = TransacaoDeposito
    exclude = ['data', 'tipo']

class TransacaoTransferenciaForm(forms.ModelForm):
  # Validacao
  #def clean(self):
  #  return self.cleaned_data

  class Meta:
    model = TransacaoTransferencia
    exclude = ['data', 'tipo']

class TransacaoSaldoForm(forms.ModelForm):
  # Validacao
  #def clean(self):
  #  return self.cleaned_data

  class Meta:
    model = Conta
    exclude = ['data', 'tipo', 'clientes', 'data_abertura', 'saldo', 'saldo_zero_desde']


