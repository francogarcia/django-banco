# <http://docs.celeryproject.org/en/latest/userguide/periodic-tasks.html>

from django.db import IntegrityError

from celery import shared_task, task
from celery.decorators import periodic_task
from celery.task import PeriodicTask
from celery.task.schedules import schedule

from datetime import date, datetime, timedelta
from dateutil import relativedelta

from decimal import *

from banco.models import Conta
from banco.models import ContaInativa
from banco.models import ContaPoupanca

import logging
g_logger = logging.getLogger(__name__)

@task(name='tasks.add_task')
def add_task(x, y):
  return x + y

@task(name='tasks.print_time_now_task')
def print_time_now_task():
  print("Time now: " + str(datetime.now()))

@periodic_task(run_every=timedelta(seconds=20))
def another_print_task():
  print("Hello!")

class SimplePeriodicTask(PeriodicTask):
  def run(self, **kwargs):
    print("Tick")

  run_every = timedelta(minutes=30)

  #@property
  #def run_every(self):
    #if datetime.now().weekday() in [1, 2, 3]:
      #return schedule(timedelta(seconds=5))
    #else:
      #return schedule(timedelta(seconds=3))

  #def is_due(self, last_run_at):
    #return (True, 1)

#SimplePeriodicTask().run()

# Requisitos Funcionais

# Requisito 8


# Requisito 13
TAXA_RENDIMENTO_POUPANCA = Decimal('0.01')
@task(name='tasks.verificar_rendimento_poupanca')
def verificar_rendimento_poupanca():
  data_atual = date.today()
  for conta in ContaPoupanca.objects.all():
    data_abertura = conta.data_abertura.date() # Converte de datetime para date.
    diferenca = relativedelta.relativedelta(data_atual, data_abertura)
    # Rendimentos no aniversario da conta (dia do mes). O ano e ignorado.
    if (diferenca.months > 0 and diferenca.days == 0):
      if (conta.saldo > 0):
        conta.saldo *= (1 + TAXA_RENDIMENTO_POUPANCA)
        try:
          # Salva o novo saldo da conta
          conta.save()
          g_logger.info("Aplicou rendimentos a conta " + conta.numero)
        except IntegrityError:
          transaction.rollback()

@task(name='tasks.verificar_contas_saldo_zero_ha_um_ano')
def verificar_contas_saldo_zero_ha_um_ano():
  print('verificar_contas_saldo_zero_ha_um_ano')
  data_atual = date.today()
  for conta in Conta.objects.all():
    saldo_zero_desde = conta.saldo_zero_desde
    if (saldo_zero_desde is not None):
      saldo_zero_desde = saldo_zero_desde.date()
      print(str(conta) + " " + str(conta.saldo_zero_desde))
      diferenca = relativedelta.relativedelta(data_atual, saldo_zero_desde)
      print("years = " + str(diferenca.years) + " months = " + str(diferenca.months) + " days = " + str(diferenca.days))
      if (diferenca.years > 1) or (diferenca.years == 1 and (diferenca.months > 0 or diferenca.days > 0)):
        try:
          conta_inativa = ContaInativa(tipo=conta.tipo,
                                       agencia=conta.agencia,
                                       clientes=conta.clientes,
                                       numero=conta.numero,
                                       data_abertura=conta.data_abertura)

          conta.delete()
          g_logger.info("Removeu a conta inativa " + conta.numero)

          conta_inativa.save()
        except IntegrityError:
          transaction.rollback()
