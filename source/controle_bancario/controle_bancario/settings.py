"""
Django settings for controle_bancario project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_PATH = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(BASE_PATH, os.pardir)
PROJECT_PATH = os.path.abspath(PROJECT_PATH)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'hrngmu(uppos^)&!07@4o7$_x=#9$wu)qs9&2=16!wwfx0j&@*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'kombu.transport.django', # Para facilitar uso de Celery
    'dateutil',
    'djcelery',
    'banco',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'controle_bancario.urls'

WSGI_APPLICATION = 'controle_bancario.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_PATH, 'db.sqlite3'),
        #'ENGINE': 'django.db.backends.mysql',
        #'NAME': 'banco',
        #'USER': 'steve',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_PATH = STATIC_URL = os.path.join(BASE_PATH, 'static/')
STATICFILES_DIRS = (
  STATIC_PATH,
)

MEDIA_ROOT = os.path.join(BASE_PATH, 'media/')

#TEMPLATE_LOADERS = (
  #'django.template.loaders.filesystem.load_template_source',
  #'django.template.loaders.banco.load_template_source',
#)

TEMPLATE_DIRS = (
  os.path.join(BASE_PATH, 'templates/'),
)

# Producao: ver STATIC_ROOT + collectstatic: <https://docs.djangoproject.com/en/dev/howto/static-files/>

# Log

import logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'debug.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'controle_bancario.custom': {
            'handlers': ['console', 'file'],
            'level': 'INFO',
        }
    },
    'root': {
        'handlers': ['console', ],
        'level': 'INFO'
    },
}

# Celery

import djcelery
djcelery.setup_loader()

from celery.schedules import crontab
from datetime import datetime, timedelta

#from banco.tasks import add_task
#from banco.tasks import print_time_now_task
from banco.tasks import verificar_rendimento_poupanca
CELERYBEAT_MAX_LOOP_INTERVAL = 15 # Tempo minimo de 15s para atualização - padrao: 5min
# <http://celery.readthedocs.org/en/latest/userguide/periodic-tasks.html#beat-entries>
CELERYBEAT_SCHEDULE = {
    #'add-every-30-seconds': {
        #'task': 'tasks.add_task',
        #'schedule': timedelta(seconds=30),
        #'args': (16, 16)
    #},
    #'print-time-every-30-seconds': {
        #'task': 'tasks.print_time_now_task',
        #'schedule': timedelta(seconds=30)
    #},
    'verificar_rendimento_poupanca-every-day': {
        'task': 'tasks.verificar_rendimento_poupanca',
        'schedule': timedelta(seconds=30)
    },
    'verificar_contas_saldo_zero_ha_um_ano-every-day': {
        'task': 'tasks.verificar_contas_saldo_zero_ha_um_ano',
        'schedule': timedelta(days=1)
    },
}

BROKER_URL = 'django://'
CELERY_TIMEZONE = TIME_ZONE
CELERYBEAT_SCHEDULE_FILENAME = BASE_PATH + '/celerybeat-schedule'
