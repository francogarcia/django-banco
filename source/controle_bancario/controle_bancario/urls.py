from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'controle_bancario.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # http://127.0.0.1:8000/
    url(r'^/', include('banco.urls')),
    # http://127.0.0.1:8000/banco/
    url(r'^banco/', include('banco.urls')),
    # http://127.0.0.1:8000/admin/
    url(r'^admin/', include(admin.site.urls)),
)
